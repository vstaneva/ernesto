/** 
 * ArduinoAccelerometer
 * by Valeria Staneva. 
 * 
 * Reads numbers from the serial port and saves them in three arrays -  
 * one for each of the X, Y, and Z-axes.
 * Then computes the X,Y,Z-coordinates relative to the acceleration,
 * assuming that the movement starts from the upper-left corner. 
 */
 

import processing.serial.*;

int bgcolor=0;			     // Background color
Serial myPort;                       // The serial port
int xAxis, yAxis, zAxis; // Store x-axis, y-axis, and z-axis data

FloatList accX; // x-acceleration; is always in [-6g; +6g]
FloatList accY; // y-acceleration; is always in [-6g; +6g]
FloatList accZ; // z-acceleration; is always in [-6g; +6g]

int t=9; // ASCII for a tab '\t'
int nl=10; // ASCII for a new line '\n'

void setup() {
  size(480, 600);  // Stage size
  //noStroke();      // No border on the next thing drawn
  accX = new FloatList();
  accY = new FloatList();
  accZ = new FloatList();
  // Print a list of the serial ports, for debugging purposes:
  printArray(Serial.list());
  // Next line chooses a port according to the OS configuration
  String portName = Serial.list()[1];
  myPort = new Serial(this, portName, 9600);
  myPort.clear();
  frameRate(10); // We record 10 times per second; if we switch to static arrays, speed could improve.
}

void draw() {
  background(bgcolor);
  getData(); // Takes input from the Serial port
  drawAcceleration(0, 0, 450, 150, 6.0, 'x', "X axis acceleration [g]"); // Upper-left:(0,0) bottom-right:(450, 150); shows the graph bos for x-axis acceleration
  drawAcceleration(0, 200, 450, 150, 6.0, 'y', "Y axis acceleration [g]"); // Upper-left:(0,200) bottom-right:(450, 350); shows the graph bos for y-axis acceleration
  drawAcceleration(0, 400, 450, 150, 6.0, 'z', "Z axis acceleration [g]"); // Upper-left:(0,400) bottom-right:(450, 550); shows the graph bos for z-axis acceleration
}

void drawAcceleration(int x, int y, int w, int h, float max, char draw, String title) {
  /// Code borrowed from Hristo Staykov (MGVarna'16), who gave us a permission to modify it
  float min=0-max;
  fill(255);
  textSize(16);
  // Print the title of the graph
  text(title, x, y+16);
  y+=20;
  h-=20;
  stroke(255);
  fill(255);
  rect(x, y, w, h);
  stroke(150);
  // Print the graph scale
  for (int i=1; i<8; i++) {
    int scaleLine=i*h/8;
    line(x, y+scaleLine, x+w, y+scaleLine);
    fill(255);
    // Text right of the graph which shows the step size of the scale
    //textSize(10);
    //text(map(i, 0, 8, max, min)+" g", x+w+5, y+scaleLine+5);
  }
  /// From here on the code is our own masterpiece
  int sizeOfArrays = accX.size();
  float sector = float(w)/float(sizeOfArrays); // How big the step between two values is
  float prev = 0, curr = 0;
  int SpaceFromLeft = x, SpaceFromTop = y + (h/2); // Top-bottom: y+(h/2) is the level of 0
  for(int i = 0; i<sizeOfArrays; i++)
  {
    // Which axis do we need data for:
    if(draw == 'x') curr = accX.get(i);
    else if (draw == 'y') curr = accY.get(i);
    else if (draw == 'z') curr = accZ.get(i);
    else curr = 0;
    // Draw a line between each two consecutive values we have
    line((sector*(i-1)) + SpaceFromLeft, ((prev*h)/12.0) + SpaceFromTop, (sector*i) + SpaceFromLeft, ((curr*h)/12.0) + SpaceFromTop);
    prev = curr;
  }
}

void getData(){
  //Get new data from the Serial port
  String barX = myPort.readStringUntil(t);
  String barY = myPort.readStringUntil(t);
  String barZ = myPort.readStringUntil(nl);
  if(barX!=null && barY!=null && barZ!=null){
    barX=barX.trim(); barY=barY.trim(); barZ=barZ.trim(); // get rid of whitespace before/after the actual number
    // xAxis, yAxis, ZAxis are numbers in [0; 1024], where 0 -> (-6g); 512 -> (0g); 1024 -> (6g)
    xAxis=int(barX);
    yAxis=int(barY);
    zAxis=int(barZ); 
    accX.append(float((xAxis-512)*6)/512.0);
    accY.append(float((yAxis-512)*6)/512.0);
    accZ.append(float((zAxis-512)*6)/512.0);
    //for debugging purposes:
    println("| "+ accX.get(accX.size()-1) + " | " + accY.get(accY.size()-1)+ " | " + accZ.get(accZ.size()-1) + " |");
  }
}
