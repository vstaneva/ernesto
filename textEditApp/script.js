﻿//Функция, отговаряща за размяната на символ с дума
function rep()
{
	var txt = document.getElementById("txtcontent").value;
	var repl = txt.replace(document.getElementById("txt1").value, document.getElementById("txt2").value); 
	//ще включа цикъл и масив за обхождане на още възможности за подмяна на символ с дума
	//alert(repl);
	document.getElementById("visual").innerHTML=repl;
}
	
//Функция, която извършва експортирането на редактирания текст в .doc формат
//Предстои да се усъвършенства (+ форматиране)
function exportDoc() 
{
	var sampletext = document.getElementById("txtcontent").value;
	//var a = document.body.appendChild(document.createElement("a"));
	document.getElementById("exportdoc").download = "exported.doc";
	document.getElementById("exportdoc").href = "data:text/plain;base64," + btoa(sampletext);
	//a.innerHTML = "download example text";
}