
/*
 ADXL3xx
 
 Reads an Analog Devices ADXL3xx accelerometer and communicates the
 acceleration to the computer.  The pins used are designed to be easily
 compatible with the breakout boards from Sparkfun, available from:
 http://www.sparkfun.com/commerce/categories.php?c=80

 http://www.arduino.cc/en/Tutorial/ADXL3xx

 The circuit:
 analog 0: accelerometer self test
 analog 1: z-axis
 analog 2: y-axis
 analog 3: x-axis
 analog 4: ground
 analog 5: vcc
 
 created 2 Jul 2008
 by David A. Mellis
 modified 30 Aug 2011
 by Tom Igoe 
 modified 27 Apr 2014
 by Andrey Georgiev

*/
// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// these constants describe the pins. They won't change:
const int groundpin = 18;             // analog input pin 4 -- ground
const int powerpin = 19;              // analog input pin 5 -- voltage
const int xpin = A3;                  // x-axis of the accelerometer
const int ypin = A2;                  // y-axis
const int zpin = A1;                  // z-axis (only on 3-axis models)

void setup()
{
  // initialize the serial communications:
  Serial.begin(9600);
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Provide ground and power by using the analog inputs as normal
  // digital pins.  This makes it possible to directly connect the
  // breakout board to the Arduino.  If you use the normal 5V and
  // GND pins on the Arduino, you can remove these lines.
  pinMode(groundpin, OUTPUT);
  pinMode(powerpin, OUTPUT);
  digitalWrite(groundpin, LOW); 
  digitalWrite(powerpin, HIGH);
}

void loop()
{
  int xAcc, yAcc, zAcc;
  xAcc = analogRead(xpin);
  yAcc = analogRead(ypin);
  zAcc = analogRead(zpin);
  // print the sensor values:
  Serial.print(xAcc);
  // print a tab between values:
  Serial.print("\t");
  Serial.print(yAcc);
  // print a tab between values:
  Serial.print("\t");
  Serial.print(zAcc);
  Serial.println();
  //Adding a delay so the received values can be read by the viewer on the LCD display:
  vdelay+=1;
  if(vdelay%25==0){
  // print the sensor values on the LCD display:
 displaySensorValues(xAcc, yAcc, zAcc);}
  // delay before next reading:
  delay(100);
}
// defining a function for printing out values on the LCD display:
void displaySensorValues(int xValue, int yValue, int zValue){
  lcd.setCursor(0, 1) ;
  lcd.print("|");
  lcd.print(xValue);
  lcd.print("|");
  lcd.print(yValue);
  lcd.print("|");
  lcd.print(zValue);
  lcd.print("|");
  }
